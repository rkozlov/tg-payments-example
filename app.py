import telebot
from telebot.types import LabeledPrice

token = '5463238363:AAHT-AcpfeSPpPbJ-bzayo0eG0bgMDx9ST8'
provider_token = '1711374395:TEST:5ec5a351b6114e62211c'
bot = telebot.TeleBot(token)

@bot.message_handler(commands=['start'])
def command_start(message):
    bot.send_message(message.chat.id,
                     "Напиши /buy для покупки открытки.\nДля оплаты используйте карту с номером `4242 4242 4242 4242` любым месяцем и CVC кодом", parse_mode='Markdown')


@bot.message_handler(commands=['buy'])
def command_pay(message):
    bot.send_invoice(
                     message.chat.id,
                     'Открытка',
                     'О́сло — столица и самый крупный город Норвегии. До 1624 года, согласно карте А. Ортелиуса 1539 года',
                     'b368bb8a2c5bdbc8e699fd4067adabbf',
                     provider_token,
                     'RUB',
                     [LabeledPrice(label='Открытка', amount=100*100)],
                     photo_url='https://cms.enjourney.ru/upload/country/article/512x512/4664_1.png',
                     photo_height=512,
                     photo_width=512,
                     is_flexible=False,
                     start_parameter='example-payments')


@bot.pre_checkout_query_handler(func=lambda query: True)
def checkout(pre_checkout_query):
    bot.answer_pre_checkout_query(pre_checkout_query.id, ok=True,
                                  error_message="Error!")


@bot.message_handler(content_types=['successful_payment'])
def got_payment(message):
    bot.send_message(message.chat.id,
                     'Отлично, перейдите по ссылке и скачайте открытку\nhttps://cdn.fishki.net/upload/post/2017/02/23/2226088/b368bb8a2c5bdbc8e699fd4067adabbf.jpg')


bot.infinity_polling(skip_pending = True)